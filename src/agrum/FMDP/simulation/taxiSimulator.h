/**
 *
 *   Copyright (c) 2005-2021 by Pierre-Henri WUILLEMIN(@LIP6) & Christophe GONZALES(@AMU)
 *   info_at_agrum_dot_org
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/**
 * @file
 * @brief Class for simulating a markov decision process.
 *
 * @author Pierre-Henri WUILLEMIN(@LIP6) and Jean-Christophe MAGNAN and Christophe
 * GONZALES(@AMU)
 */
#ifndef GUM_TAXI_SIMULATOR_H
#define GUM_TAXI_SIMULATOR_H
//======================================================================
#include <agrum/tools/multidim/instantiation.h>
//======================================================================
#include <agrum/FMDP/fmdp.h>
//======================================================================
#include <agrum/FMDP/simulation/abstractSimulator.h>
//======================================================================
#include <agrum/tools/variables/labelizedVariable.h>
//======================================================================

namespace gum {


  enum TaxiSimulationLandmark : Idx
  {
    HOME    = 0,
    WORK    = 1,
    THEATER = 2,
    CLUB    = 3,
    TAXI    = 4
  };
  enum TaxiSimulationLandmarkX : Idx
  {
    HOMEX    = 0,
    WORKX    = 0,
    THEATERX = 3,
    CLUBX    = 4,
    STATIONX = 2
  };
  enum TaxiSimulationLandmarkY : Idx
  {
    HOMEY    = 0,
    WORKY    = 4,
    THEATERY = 0,
    CLUBY    = 4,
    STATIONY = 1
  };
  enum TaxiSimulationAction : Idx
  {
    GoNorth = 1,
    GoEast  = 2,
    GoSouth = 3,
    GoWest  = 4,
    PickUp  = 5,
    PutDown = 6,
    FillUp  = 7
  };

  // clang-format off
  /**
* @class TaxiSimulator
* @headerfile taxiSimulator.h <agrum/FMDP/simulation/taxiSimulator.h>
   * @brief A class to simulate the Taxi problem
   * @ingroup fmdp_group
   */
  // clang-format on
  class TaxiSimulator: public AbstractSimulator {
    public:
    // ===========================================================================
    /// @name Constructors, Destructors.
    // ===========================================================================
    /// @{

    /**
     * Default constructor.
     */
    TaxiSimulator();

    /**
     * Default destructor.
     */
    ~TaxiSimulator();

    /// @}

    // ===========================================================================
    /// @name States
    // ===========================================================================
    /// @{

    protected:
    /// Choses a random state as the first test for a run
    Instantiation randomState_();

    public:
    bool hasReachEnd();

    /// @}

    // ===========================================================================
    /// @name Variables
    // ===========================================================================
    /// @{

    const DiscreteVariable* primeVar(const DiscreteVariable* mainVar) {
      return primeMap__.second(mainVar);
    }

    /// Iteration over the variables of the simulated probleme
    SequenceIteratorSafe< const DiscreteVariable* > beginVariables() {
      return taxiVars__.beginSafe();
    }
    SequenceIteratorSafe< const DiscreteVariable* > endVariables() {
      return taxiVars__.endSafe();
    }

    /// @}

    // ===========================================================================
    /// @name Actions
    // ===========================================================================
    /// @{

    const std::string& actionName(Idx actionId) { return *actionMap__[actionId]; }

    /// Iteration over the variables of the simulated probleme
    SequenceIteratorSafe< Idx > beginActions() {
      return taxiActions__.beginSafe();
    }
    SequenceIteratorSafe< Idx > endActions() { return taxiActions__.endSafe(); }


    void perform(Idx);

    private:
    void performGoNorth__();
    void performGoEast__();
    void performGoSouth__();
    void performGoWest__();
    void performPickUp__();
    void performPutDown__();
    void performFillUp__();

    /// @}

    // ===========================================================================
    /// @name Rewards
    // ===========================================================================
    /// @{
    public:
    double reward();

    private:
    void evalReward__();
    bool isAtDestination__(TaxiSimulationLandmark  passDest,
                           TaxiSimulationLandmarkX xCurPos,
                           TaxiSimulationLandmarkY yCurPos);
    bool isAtMeetPoint__(TaxiSimulationLandmark  passpos,
                         TaxiSimulationLandmarkX xCurPos,
                         TaxiSimulationLandmarkY yCurPos);

    /// @}

    /// Variables data structures
    Sequence< const DiscreteVariable* >                           taxiVars__;
    Bijection< const DiscreteVariable*, const DiscreteVariable* > primeMap__;
    LabelizedVariable*                                            xPos__;
    LabelizedVariable*                                            yPos__;
    LabelizedVariable*                                            passengerPos__;
    LabelizedVariable*                                            passengerDest__;
    LabelizedVariable*                                            fuelLevel__;

    /// Actions
    Sequence< Idx >                taxiActions__;
    HashTable< Idx, std::string* > actionMap__;   //__actionMap.insert ( actionId,
    // new std::string ( action ) );
    TaxiSimulationAction lastAction__;

    /// Reward
    double reward__;
  };

} /* namespace gum */


#endif   // GUM_TAXI_SIMULATOR_H
