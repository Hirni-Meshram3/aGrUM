/**
 *
 *   Copyright (c) 2005-2021 by Pierre-Henri WUILLEMIN(@LIP6) & Christophe GONZALES(@AMU)
 *   info_at_agrum_dot_org
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/**
 * @file
 * @brief Inline implementation of the Pattern class.
 *
 * @author Lionel TORTI and Pierre-Henri WUILLEMIN(@LIP6)
 */

namespace gum {
  namespace prm {
    namespace gspan {

      INLINE
      Pattern::Pattern() : DiGraph(), last__(0) {
        GUM_CONSTRUCTOR(Pattern);
        ;
      }

      INLINE
      Pattern::~Pattern() {
        GUM_DESTRUCTOR(Pattern);
        ;
      }

      INLINE
      NodeId Pattern::addNodeWithLabel(LabelData& l) {
        NodeId n = NodeId(size() + 1);
        DiGraph::addNodeWithId(n);
        node_map__.insert(n, &l);
        last__ = &l;
        return n;
      }

      INLINE
      LabelData& Pattern::label(NodeId node) {
        try {
          return *(node_map__[node]);
        } catch (NotFound&) {
          GUM_ERROR(NotFound, "node not found in this Pattern")
        }
      }

      INLINE
      const LabelData& Pattern::label(NodeId node) const {
        try {
          return *(node_map__[node]);
        } catch (NotFound&) {
          GUM_ERROR(NotFound, "node not found in this Pattern")
        }
      }

      INLINE
      LabelData& Pattern::lastAdded() {
        if (last__) return *last__;

        GUM_ERROR(OperationNotAllowed, "there are no LabelData yet")
      }

      INLINE
      const LabelData& Pattern::lastAdded() const {
        if (last__) return *last__;

        GUM_ERROR(OperationNotAllowed, "there are no LabelData yet")
      }

      INLINE
      LabelData& Pattern::label(NodeId i, NodeId j) {
        try {
          return *(arc_map__[Arc(i, j)].first);
        } catch (NotFound&) {
          GUM_ERROR(NotFound, "arc not found in this Pattern")
        }
      }

      INLINE
      const LabelData& Pattern::label(NodeId i, NodeId j) const {
        try {
          return *(arc_map__[Arc(i, j)].first);
        } catch (NotFound&) {
          GUM_ERROR(NotFound, "arc not found in this Pattern")
        }
      }

      INLINE
      LabelData& Pattern::label(const Arc& arc) {
        try {
          return *(arc_map__[arc].first);
        } catch (NotFound&) {
          GUM_ERROR(NotFound, "arc not found in this Pattern")
        }
      }

      INLINE
      const LabelData& Pattern::label(const Arc& arc) const {
        try {
          return *(arc_map__[arc].first);
        } catch (NotFound&) {
          GUM_ERROR(NotFound, "arc not found in this Pattern")
        }
      }

      INLINE
      void Pattern::addArc(NodeId i, NodeId j, LabelData& l) {
        if (!(DiGraph::exists(i) && DiGraph::exists(j))) {
          GUM_ERROR(NotFound, "node not found in this pattern")
        }

        EdgeCode* edge
           = new EdgeCode(i, j, node_map__[i]->id, l.id, node_map__[j]->id);

        if ((code().codes.size() == 0)
            || (DFSCode::validNeighbors(code().codes.back(), edge))) {
          DiGraph::addArc(i, j);
          arc_map__.insert(Arc(i, j), std::make_pair(&l, edge));
          code().codes.push_back(edge);
        } else {
          delete edge;
          GUM_ERROR(OperationNotAllowed,
                    "illegal arc considering neighborhood restriction")
        }
      }

      INLINE
      bool Pattern::exists(NodeId id) const { return DiGraph::exists(id); }

      INLINE
      bool Pattern::exists(NodeId tail, NodeId head) const {
        return DiGraph::existsArc(tail, head);
      }

      INLINE
      Size Pattern::size() const { return DiGraph::size(); }

      INLINE
      Size Pattern::sizeArcs() const { return DiGraph::sizeArcs(); }

      INLINE
      void Pattern::rightmostPath(std::list< NodeId >& r_path) const {
        r_path.push_back(NodeId(size()));

        while (r_path.front() != 1) {
          for (const auto par: parents(r_path.front())) {
            if (par < r_path.front()) {
              r_path.push_front(par);
              break;
            }
          }
        }
      }

      INLINE const NodeGraphPart& Pattern::nodes() const {
        return DiGraph::nodes();
      }

      INLINE const ArcSet& Pattern::arcs() const { return DiGraph::arcs(); }

      INLINE
      DFSCode& Pattern::code() { return code__; }

      INLINE
      const DFSCode& Pattern::code() const { return code__; }

      INLINE
      EdgeCode& Pattern::edgeCode(NodeId tail, NodeId head) {
        try {
          return *(arc_map__[Arc(tail, head)].second);
        } catch (NotFound&) { GUM_ERROR(NotFound, "arc not found in Pattern") }
      }

      INLINE
      EdgeCode& Pattern::edgeCode(const Arc& arc) {
        try {
          return *(arc_map__[arc].second);
        } catch (NotFound&) { GUM_ERROR(NotFound, "arc not found in Pattern") }
      }

      INLINE
      const EdgeCode& Pattern::edgeCode(NodeId tail, NodeId head) const {
        try {
          return *(arc_map__[Arc(tail, head)].second);
        } catch (NotFound&) { GUM_ERROR(NotFound, "arc not found in Pattern") }
      }

      INLINE
      const EdgeCode& Pattern::edgeCode(const Arc& arc) const {
        try {
          return *(arc_map__[arc].second);
        } catch (NotFound&) { GUM_ERROR(NotFound, "arc not found in Pattern") }
      }

      INLINE
      void Pattern::pop_back() {
        EdgeCode* edge = code__.codes.back();
        code__.codes.pop_back();

        if (edge->isForward()) {
          node_map__.erase(edge->j);
          arc_map__.erase(Arc(edge->i, edge->j));
          DiGraph::eraseArc(Arc(edge->i, edge->j));
          DiGraph::eraseNode(edge->j);
        } else {
          arc_map__.erase(Arc(edge->i, edge->j));
          DiGraph::eraseArc(Arc(edge->i, edge->j));
        }

        delete edge;
      }

      INLINE
      void Pattern::remove(NodeId node) {
        if (DiGraph::parents(node).empty() && DiGraph::children(node).empty()) {
          DiGraph::eraseNode(node);
          node_map__.erase(node);
        } else {
          GUM_ERROR(OperationNotAllowed, "the given node has neighbors")
        }
      }
    } /* namespace gspan */
  }   /* namespace prm */
} /* namespace gum */
