/**
 *
 *   Copyright (c) 2005-2021 by Pierre-Henri WUILLEMIN(@LIP6) & Christophe GONZALES(@AMU)
 *   info_at_agrum_dot_org
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/**
 * @file
 * @brief A Interface to all Causal Independence models
 *
 * Causal Independence (CI) is a method of defining a discrete distribution
 * that can dramatically reduce the number of prior probabilities necessary to
 * define a distribution.
 *
 * @author Pierre-Henri WUILLEMIN(@LIP6) & Christophe GONZALES(@AMU)
 */
#include <agrum/tools/core/bijection.h>
#include <agrum/tools/multidim/ICIModels/multiDimICIModel.h>
#include <agrum/tools/multidim/implementations/multiDimReadOnly.h>

namespace gum {

  // Default constructor
  template < typename GUM_SCALAR >
  INLINE
     MultiDimICIModel< GUM_SCALAR >::MultiDimICIModel(GUM_SCALAR external_weight,
                                                      GUM_SCALAR default_weight) :
      MultiDimReadOnly< GUM_SCALAR >(),
      external_weight__(external_weight), default_weight__(default_weight) {
    GUM_CONSTRUCTOR(MultiDimICIModel);
  }

  // Default constructor
  template < typename GUM_SCALAR >
  INLINE MultiDimICIModel< GUM_SCALAR >::MultiDimICIModel(
     const MultiDimICIModel< GUM_SCALAR >& from) :
      MultiDimReadOnly< GUM_SCALAR >(from) {
    GUM_CONS_CPY(MultiDimICIModel);
    default_weight__  = from.default_weight__;
    external_weight__ = from.external_weight__;
    causal_weights__  = from.causal_weights__;
  }

  // Copy constructor using a bijection to replace variables from source.
  template < typename GUM_SCALAR >
  INLINE MultiDimICIModel< GUM_SCALAR >::MultiDimICIModel(
     const Bijection< const DiscreteVariable*, const DiscreteVariable* >& bij,
     const MultiDimICIModel< GUM_SCALAR >&                                from) :
      MultiDimReadOnly< GUM_SCALAR >() {
    GUM_CONSTRUCTOR(MultiDimICIModel);
    default_weight__  = from.default_weight__;
    external_weight__ = from.external_weight__;

    for (HashTableConstIteratorSafe< const DiscreteVariable*, GUM_SCALAR > iter
         = from.causal_weights__.beginSafe();
         iter != from.causal_weights__.endSafe();
         ++iter) {
      try {
        causalWeight(*(bij.first(iter.key())), iter.val());
      } catch (NotFound&) { causalWeight(*(iter.key()), iter.val()); }
    }
  }

  // destructor
  template < typename GUM_SCALAR >
  INLINE MultiDimICIModel< GUM_SCALAR >::~MultiDimICIModel() {
    GUM_DESTRUCTOR(MultiDimICIModel);
  }

  template < typename GUM_SCALAR >
  INLINE GUM_SCALAR MultiDimICIModel< GUM_SCALAR >::causalWeight(
     const DiscreteVariable& v) const {
    return (causal_weights__.exists(&v)) ? causal_weights__[&v] : default_weight__;
  }

  template < typename GUM_SCALAR >
  INLINE void
     MultiDimICIModel< GUM_SCALAR >::causalWeight(const DiscreteVariable& v,
                                                  GUM_SCALAR w) const {
    if (!this->contains(v)) {
      GUM_ERROR(InvalidArgument, v.name() << " is not a cause for this CI Model")
    }

    if (w == (GUM_SCALAR)0) {
      GUM_ERROR(gum::OutOfBounds, "causal weight in CI Model>0")
    }

    causal_weights__.set(&v, w);
  }

  template < typename GUM_SCALAR >
  INLINE GUM_SCALAR MultiDimICIModel< GUM_SCALAR >::externalWeight() const {
    return external_weight__;
  }

  template < typename GUM_SCALAR >
  INLINE void MultiDimICIModel< GUM_SCALAR >::externalWeight(GUM_SCALAR w) const {
    external_weight__ = w;
  }

  template < typename GUM_SCALAR >
  std::string MultiDimICIModel< GUM_SCALAR >::toString() const {
    std::stringstream s;
    s << this->variable(0) << "=CIModel([" << externalWeight() << "],";

    for (Idx i = 1; i < this->nbrDim(); i++) {
      s << this->variable(i) << "[" << causalWeight(this->variable(i)) << "]";
    }

    s << ")";

    std::string res;
    s >> res;
    return res;
  }
  template < typename GUM_SCALAR >
  void MultiDimICIModel< GUM_SCALAR >::copyFrom(
     const MultiDimContainer< GUM_SCALAR >& src) const {
    auto p = dynamic_cast< const MultiDimICIModel< GUM_SCALAR >* >(&src);
    if (p == nullptr)
      MultiDimReadOnly< GUM_SCALAR >::copyFrom(src);
    else {
      if (src.domainSize() != this->domainSize()) {
        GUM_ERROR(OperationNotAllowed, "Domain sizes do not fit")
      }
      external_weight__ = p->external_weight__;
      default_weight__  = p->default_weight__;
      for (Idx i = 1; i < this->nbrDim(); i++) {
        causal_weights__.set(
           const_cast< const DiscreteVariable* >(&this->variable(i)),
           p->causalWeight(this->variable(i)));
      }
    }
  }

  // returns the name of the implementation
  template < typename GUM_SCALAR >
  INLINE const std::string& MultiDimICIModel< GUM_SCALAR >::name() const {
    static const std::string str = "MultiDimICIModel";
    return str;
  }

  template < typename GUM_SCALAR >
  INLINE void MultiDimICIModel< GUM_SCALAR >::replace_(const DiscreteVariable* x,
                                                       const DiscreteVariable* y) {
    MultiDimReadOnly< GUM_SCALAR >::replace_(x, y);
    causal_weights__.insert(y, causal_weights__[x]);
    causal_weights__.erase(x);
  }

} /* namespace gum */
